
// define STYLE
#define PROGRESS_STYLE_TEXT 0
#define PROGRESS_STYLE_CHAR 1
#define PROGRESS_STYLE_RGB  2

// define font background color
#define RGB_BACK_BLACK  40
#define RGB_BACK_RED    41
#define RGB_BACK_GREEN  42
#define RGB_BACK_YELLOW 43
#define RGB_BACK_BLUE   44
#define RGB_BACK_PURPLE 45
#define RGB_BACK_WRITE  47

//define font color
#define RGB_FONT_BLACK  30
#define RGB_FONT_RED    31
#define RGB_FONT_GREEN  32
#define RGB_FONT_YELLOW 33
#define RGB_FONT_BLUE   34
#define RGB_FONT_PURPLE 35
#define RGB_FONT_WRITE  37

//define error handle
/*#define err(x,y)
{
	perror(x);
	exit(y);
}*/


struct Progress_bar
{
	char chr;	 //,if you are PROGRESS_STYLE_CHAR,display in chr     
	int font_color;  //font color
	int back_color;  //display in PROGRESS_STYLE_RGB
	int style;	
	int max;	   
	float offset;
	char *pro;
};

extern void Init_Progress(struct Progress_bar* pro,int style,int max);
extern void Show_Progress(struct Progress_bar* bar,int bit);
extern void Free_Progress(struct Progress_bar *bar);
extern void setBackcolor(struct Progress_bar* bar,int back_color);
extern void setFontcolor(struct Progress_bar* bar,int font_color);
extern void setChar(struct Progress_bar* bar,int chr);