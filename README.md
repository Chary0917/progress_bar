linux 下终端进度条的简单封装，C语言实现。有三种模式：
1.PROGRESS_STYLE_TEXT
2.PROGRESS_STYLE_CHAR
3.PROGRESS_STYLE_RGB

接口：
初始化：
Init_Progress(struct Progress_bar* pro,int style,int max)
显示：
Show_Progress(struct Progress_bar* bar,int bit)
修改：
setBackcolor(struct Progress_bar* bar,int back_color) //修改背景颜色
setFontcolor(struct Progress_bar* bar,int font_color) //修改字体颜色
setChar(struct Progress_bar* bar,int chr) //修改PROGRESS_STYLE_CHAR模式的显示字符
释放：
Free_Progress(struct Progress_bar *bar);