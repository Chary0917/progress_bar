#include "Progress.h"
#include <unistd.h>

int main(int argc, char const *argv[])
{
	struct Progress_bar bar;
	Init_Progress(&bar,PROGRESS_STYLE_CHAR,100);
	setChar(&bar,'+');
	setFontcolor(&bar,RGB_FONT_YELLOW);
	setBackcolor(&bar,RGB_BACK_PURPLE);
	int i=0;
	for(;i<=100;i++){
		Show_Progress(&bar,i);
		sleep(1);
	}
	
	Free_Progress(&bar);
	return 0;
}