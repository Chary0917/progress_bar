#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Progress.h"


void Init_Progress(struct Progress_bar* pro,int style,int max)
{
	pro->style=style;
	pro->max=max;
	pro->chr='#';
	pro->pro=malloc(max+1);
	pro->offset = 100 / (float)max;
	pro->back_color=RGB_BACK_BLACK;
	pro->font_color=RGB_FONT_WRITE;

	if (style != PROGRESS_STYLE_CHAR ){
		memset(pro->pro,'\0',max+1);
	} else {
		memset(pro->pro,' ',max);
		memset(pro->pro+max,'\0',1);
	}

}

void Show_Progress(struct Progress_bar* bar,int bit)
{
	int val=(int)(bit * bar->offset);
	switch(bar->style){
		case PROGRESS_STYLE_TEXT:
				printf("\033[?25l\033[%dm\033[1m%d%%\033[?25h\033[0m\r",
						bar->font_color,(int)(bar->offset * val));
				fflush(stdout);
				break;
		case PROGRESS_STYLE_CHAR:
				memset(bar->pro, bar->chr, val);
				printf("\033[?25l\033[%dm\033[1m[%-s] %d%%\033[?25h\033[0m\r",
						bar->font_color,bar->pro, (int)(bar->offset * val));
				fflush(stdout);
				break;
		case PROGRESS_STYLE_RGB:
				memset(bar->pro, ' ', val);
				printf("\033[?25l\033[%dm\033[1m\033[%dm %d%% %s\033[?25h\033[0m\r",
						bar->font_color,bar->back_color,(int)(bar->offset * val), bar->pro);
				fflush(stdout);
				break;
	} 
}

void setChar(struct Progress_bar* bar,int chr)
{
	bar->chr=chr;
}

void setFontcolor(struct Progress_bar* bar,int font_color)
{
	bar->font_color=font_color;
}

void setBackcolor(struct Progress_bar* bar,int back_color)
{
	bar->back_color=back_color;
}


void Free_Progress(struct Progress_bar *bar)
{
	free(bar->pro);
}